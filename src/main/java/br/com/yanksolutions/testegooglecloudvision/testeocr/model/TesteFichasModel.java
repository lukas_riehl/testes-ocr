package br.com.yanksolutions.testegooglecloudvision.testeocr.model;

import io.github.funpad.poi.ss.model.annotations.SheetColumn;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "teste_fichas_sicoob")
public class TesteFichasModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    private long id;

    @Column(name = "conteudo_pdf", columnDefinition = "TEXT")
    private String conteudoPdf;

    @SheetColumn("NOME")
    @Column(name = "nome", length = 100)
    private String nome;

    @SheetColumn("TIPO")
    @Column(name = "tipo", length = 10)
    private String tipo;

    @SheetColumn("CPF/CNPJ")
    @Column(name = "cpf_cnpj", length = 30)
    private String cpfCnpj;

    @SheetColumn("ASSINATURA")
    @Column(name = "assinatura", length = 100)
    private String assinatura;

    @SheetColumn("ARQUIVO")
    @Column(name = "caminho_arquivo", length = 300)
    private String caminhoArquivo;

    @SheetColumn("ASSINATURA PARECIDA COM NOME")
    @Column(name = "assin_parec_nome", length = 3)
    private String assinParecNome;

    @Override
    public String toString() {
        return "TesteFichasModel{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", tipo='" + tipo + '\'' +
                ", cpfCnpj='" + cpfCnpj + '\'' +
                ", assinatura='" + assinatura + '\'' +
                ", caminhoArquivo='" + caminhoArquivo + '\'' +
                ", assinParecNome='" + assinParecNome + '\'' +
                '}';
    }
}
