package br.com.yanksolutions.testegooglecloudvision.testeocr.repository;

import br.com.yanksolutions.testegooglecloudvision.testeocr.model.TesteFichasModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TesteFichasRepository extends JpaRepository<TesteFichasModel, Long> {

    Optional<TesteFichasModel> findDistinctByCpfCnpj(String cpfCnpj);

    Optional<TesteFichasModel> findDistinctByCaminhoArquivo(String caminhoArquivo);
}
