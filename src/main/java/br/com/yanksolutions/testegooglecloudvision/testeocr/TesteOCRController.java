package br.com.yanksolutions.testegooglecloudvision.testeocr;

import br.com.yank.web.seleniumutils.Navegador;
import br.com.yank.web.seleniumutils.SeleniumWebUtils;
import br.com.yanksolutions.testegooglecloudvision.testeocr.model.TesteFichasModel;
import br.com.yanksolutions.testegooglecloudvision.testeocr.service.TesteFichasService;
import com.gargoylesoftware.htmlunit.WebClient;
import com.google.api.gax.core.FixedCredentialsProvider;
import com.google.auth.Credentials;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.vision.v1.*;
import com.google.protobuf.ByteString;
import io.github.funpad.poi.ss.writer.SpreadsheetWriter;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.text.similarity.LevenshteinDistance;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import utils.TextoUtils;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiredArgsConstructor
@Controller
public class TesteOCRController {

    private final TesteFichasService testeFichasService;

    @Value("${diretorio-arquivos}")
    private String diretorioArquivos;

    @Value("${planilha-resultados}")
    private String diretorioResultado;

    private WebClient webClient;

    private static final String REGEX_CPF_CNPJ = "^(\\d{2}\\.?\\d{3}\\.?\\d{3}\\/?" + "\\d{4}-?\\d{2}|\\d{3}\\.?\\d{3}\\.?\\d{3}-?\\d{2})$";

    private static final double PC_SIMILARIDADE = 0.6;

    private static final double PC_SIMILARIDADE_NOME_E_ASSINATURA = 0.9;

    //@PostConstruct
    public void analisarPDFs() {
        try {
            Navegador navegador = SeleniumWebUtils.getNavegador();

            List<File> listArquivos = Files.find(Paths.get(diretorioArquivos), 999, (p, bfa) -> bfa.isRegularFile() && p.getFileName().toString().toUpperCase().endsWith(".PDF")).map(p -> p.toFile()).sorted(Comparator.comparing(p -> new Date(p.lastModified()))).collect(Collectors.toList());

            if (!listArquivos.isEmpty()) {
                listArquivos.forEach(arq -> {
//                    if (arq.getName().equalsIgnoreCase("SANDRA MARIA.pdf")
//                            || arq.getName().equalsIgnoreCase("AMANDA RIBEIRO.pdf")) {
                    TesteFichasModel testeFichasModel = testeFichasService.findDistinctByCaminhoArquivo(arq.getName());

                    if (Objects.nonNull(testeFichasModel) && testeFichasModel.getId() > 0L) {
                        System.out.println("PDF " + arq.getName() + "já foi processado!");
                    } else {
                        System.out.println("Iniciando a leitura do PDF: " + arq.getName());

                        try {
                            String textoPdf = extrairTextoCompletoPDF(arq);

                            System.out.println(textoPdf);

                            gravarDadosFicha(arq.getName(), textoPdf);
                        } catch (IOException e) {
                            System.out.println("Falha ao extrair texto do PDF " + arq.getName() + "!" + "Detalhes: " + ExceptionUtils.getStackTrace(e));
                        }

                        System.out.println("Fim da leitura do PDF: " + arq.getName());
//                    }
                    }
                });
            }

            gerarPlanilhaResultados();
        } catch (Exception ex) {
            ExceptionUtils.getStackTrace(ex);
        }
    }

    //@PostConstruct
    public void teste() {
        String textoFicha = null;
        try {
            textoFicha = extrairTextoCompletoPDF(new File(Objects.requireNonNull(this.getClass().getClassLoader()
                    .getResource("laudos-crediponto/novos/6345689_LAUDO_COM_VAGA_SEPARADA.pdf")).toURI()));

            System.out.println("Texto Ficha:\n\n\n");
            System.out.println(textoFicha);
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }

        System.out.println("Texto Ficha:\n\n\n");
        System.out.println(textoFicha);
    }

    @PostConstruct
    public void analisarPdf() {
        try {
            File dirLaudos = new File(Objects.requireNonNull(this.getClass().getClassLoader().getResource("laudos-crediponto")).toURI());

            File[] laudos = dirLaudos.listFiles(new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return name.toLowerCase().endsWith(".txt");
                }
            });

            if (Objects.nonNull(laudos)) {
                Stream.of(laudos).forEach(l -> {
                    try {
//                        String textoFicha = extrairTextoCompletoPDF(new File(Objects.requireNonNull(this.getClass().getClassLoader().getResource("laudos-crediponto/com_itau_vp_servlets2.pdf")).toURI()));
//
//                        byte[] strToBytes = textoFicha.getBytes();

//                        Path path = Paths.get(l.getAbsolutePath().replace(".pdf", ".txt"));
//
//                        Files.write(path, strToBytes);
//
//                        System.out.println("Texto Ficha:\n\n\n");
//                        System.out.println(textoFicha);

                        //String[] linhas = textoFicha.split("\n");

                        Path path = Paths.get(l.getAbsolutePath().replace(".pdf", ".txt"));

                        List<String> linhas = Files.readAllLines(path);

                        Map<Integer, String> linhasIntervalo = new HashMap<>();

                        boolean encontrouPadrao = false;

                        String linhaAtual;

                        int maxLinha = -1;

                        for (int i = 0; i < linhas.size(); i++) {
                            linhaAtual = linhas.get(i);

                            if (encontrouPadrao) {
                                if (TextoUtils.isPadraoEncontrado(linhaAtual, "^P[aá]gina\\s?\\d+\\s?\\/\\s?\\d+")) {
                                    maxLinha = i;

                                    break;
                                }

                                linhasIntervalo.put(i, linhaAtual);
                            }

                            //if (TextoUtils.isPadraoEncontrado(linhaAtual, "^Valor\\sEdifica[çc][aã]o:\\s?R[S$]\\s?\\d+\\..+?(?=$)"))
                            if (TextoUtils.isPadraoEncontrado(linhaAtual,
                                    "(Valor\\sde\\sMercado\\sTotal\\sdo\\sIm[óo]+vel\\:)" +
                                            "|(Valor\\sFinal\\sda\\sAvalia[çc]+[aã]+o\\sconforme\\so\\sMercado\\:)"))
                                encontrouPadrao = true;
                        }

                        if (encontrouPadrao && maxLinha >= 1) {
                            System.out.println("Valor do imóvel para o laudo " + l.getName() + " : " + linhasIntervalo.get(maxLinha - 1));
                        }
                    } catch (IOException e) {
                        System.out.println(ExceptionUtils.getStackTrace(e));
                    }
                });
            }


            //OutputStream outStream = new FileOutputStream(fileSaida);

//            byte[] buffer = new byte[8 * 1024];
//            int bytesRead;
//            while ((bytesRead = is.read(buffer)) != -1) {
//                outStream.write(buffer, 0, bytesRead);

            //byte[] dados = IOUtils.toByteArray(Objects.requireNonNull(is));

//            outStream.write(dados);
//
//            IOUtils.closeQuietly(outStream);

            //BufferedImage bim = ImageIO.read(is);


//            String textoFicha = extrairTextoCompletoPDF(new File(Objects.requireNonNull(this.getClass().getClassLoader().getResource("laudos-crediponto/com_itau_vp_servlets2.pdf")).toURI()));
//
//            System.out.println("Texto Ficha:\n\n\n");
//            System.out.println(textoFicha);

//            File laudoPng;
//
//            try(PDDocument document = PDDocument.load(new File(Objects.requireNonNull(this.getClass().getClassLoader()
//                    .getResource("laudos-crediponto/6204642 - laudo eletrônico.pdf")).toURI()))) {
//
//                PDFRenderer pdfRenderer = new PDFRenderer(document);
//
//                laudoPng = new File(this.getClass().getClassLoader()
//                        .getResource("laudos-crediponto/6204642 - laudo eletrônico.pdf").toURI().getPath().replace(".pdf", ".png"));
//
//                for (int page = 0; page < document.getNumberOfPages(); ++page) {
//                    if(page != 0)
//                        continue;
//
//                    BufferedImage bim = pdfRenderer.renderImageWithDPI(
//                            page, 300, ImageType.RGB);
//                    ImageIOUtil.writeImage(
//                            bim, laudoPng.getPath(), 300);
//                }
//            }
//
//            byte[] fileContent = Files.readAllBytes(laudoPng.toPath());
//            String conteudoBase64 = Base64.getEncoder().encodeToString(fileContent);
//
//            //Unirest.setTimeouts(0, 0);
//            HttpResponse<JsonNode> response = Unirest.post("http://localhost:8008/api/v1/ocr/post/ocrspace")
//                    .header("Content-Type", "application/x-www-form-urlencoded")
//                    .field("empresa", "TESTE")
//                    .field("usuario", "teste")
//                    .field("senha", "teste@123")
//                    .field("tipo", "png")
//                    .field("base64", conteudoBase64)
//                    .asJson();
//
//            System.out.println(response.getBody().toString());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    //@PostConstruct
    public void analisarFichaTransjordano() {
        try (InputStream is = this.getClass().getClassLoader().getResourceAsStream("ficha-transjordano/ficha-transjordano.jpeg")) {
            File fileSaida = new File("c:\\dev\\ficha-transjordano.jpg");

            OutputStream outStream = new FileOutputStream(fileSaida);

//            byte[] buffer = new byte[8 * 1024];
//            int bytesRead;
//            while ((bytesRead = is.read(buffer)) != -1) {
//                outStream.write(buffer, 0, bytesRead);

            byte[] dados = IOUtils.toByteArray(Objects.requireNonNull(is));

            outStream.write(dados);

            IOUtils.closeQuietly(outStream);

            BufferedImage bim = ImageIO.read(fileSaida);

            String textoFicha = aplicarOcr(dados, bim);

            System.out.println("Texto Ficha:\n\n\n");
            System.out.println(textoFicha);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String extrairDado(String campo, String nomeRazaoSocial, String textoPDF) {
        String[] linhas = textoPDF.split("\n");

        if (linhas.length > 0) {
            String ultLinha = null;
            String valor;
            int indLinha = 0;
            int indLinEnc = -1;

            List<String> listLinhas = Arrays.asList(linhas);

            for (String linha : listLinhas) {
                try {
                    switch (campo) {
                        case "CPF_CNPJ":
                            if (linha.equalsIgnoreCase("CPF") || linha.equalsIgnoreCase("CNPJ")) {
                                ultLinha = linha;
                            } else if (StringUtils.isNotEmpty(ultLinha) && Arrays.asList("CPF", "CNPJ").contains(ultLinha.toUpperCase())) {
                                valor = aplicarRegex(linha, REGEX_CPF_CNPJ);

                                if (StringUtils.isNotEmpty(valor)) return valor.toUpperCase();
                            }

                            break;

                        case "NOME_RAZAO_SOCIAL":
                            if (linha.equalsIgnoreCase("NOME COMPLETO") || linha.equalsIgnoreCase("RAZÃO SOCIAL COMPLETA")) {
                                ultLinha = linha;

                                indLinEnc = indLinha;
                            } else if (StringUtils.isNotEmpty(ultLinha) && Arrays.asList("NOME COMPLETO", "RAZÃO SOCIAL COMPLETA").contains(ultLinha.toUpperCase()) && indLinha - indLinEnc == 2) {
                                if (StringUtils.isNotEmpty(linha)) return linha.toUpperCase();
                            }

                            break;

                        case "ASSINATURA":
                            if (encontrouNomeAposAssinatura(linha.trim(), nomeRazaoSocial)) {
                                indLinEnc = indLinha;

                                if (indLinEnc > 0) {
                                    valor = listLinhas.get(indLinEnc - 1);

                                    if (StringUtils.isNotEmpty(valor) && !Arrays.asList("CPF", "CNPJ").contains(valor.toUpperCase())) {
                                        if (indLinEnc > 1 && valor.trim().equalsIgnoreCase(".") && StringUtils.isNotEmpty(listLinhas.get(indLinEnc - 2))) {
                                            return valor.toUpperCase().concat(" ").concat(listLinhas.get(indLinEnc - 2).trim());
                                        } else {
                                            return valor.toUpperCase();
                                        }
                                    } else indLinEnc = -1;
                                } else return "";
                            }

                            break;

                        default:

                            break;
                    }


                } catch (Exception ex) {

                } finally {
                    indLinha++;
                }
            }
        }

        return "";
    }

    public void gravarDadosFicha(String nomeArquivo, String textoPDF) {
        try {
            String cpfCnpj = extrairDado("CPF_CNPJ", "", textoPDF);

            if (StringUtils.isNotEmpty(cpfCnpj)) {
                String cpfCnpjSemPontuacao = cpfCnpj.replaceAll("[^0-9]", "");

                TesteFichasModel testeFichasModel = testeFichasService.findDistinctByCpfCnpj(cpfCnpjSemPontuacao);

                testeFichasModel.setCpfCnpj(cpfCnpjSemPontuacao);

                testeFichasModel.setTipo(cpfCnpjSemPontuacao.length() == 11 ? "CPF" : "CNPJ");

                testeFichasModel.setNome(extrairDado("NOME_RAZAO_SOCIAL", "", textoPDF));
                testeFichasModel.setAssinatura(extrairDado("ASSINATURA", testeFichasModel.getNome(), textoPDF));

                if (StringUtils.isNotEmpty(testeFichasModel.getAssinatura()) && StringUtils.isNotEmpty(aplicarRegex(testeFichasModel.getAssinatura(), "y:\\s?\\d+")))
                    testeFichasModel.setAssinatura("");

                testeFichasModel.setCaminhoArquivo(nomeArquivo);
                testeFichasModel.setAssinParecNome(assinaturaParecidaComNome(testeFichasModel.getNome(), testeFichasModel.getAssinatura()) ? "SIM" : "NÃO");
                testeFichasModel.setConteudoPdf(textoPDF);

                testeFichasService.saveOrUpdate(testeFichasModel.getId() == 0L ? null : testeFichasModel.getId(), testeFichasModel);
            }
        } catch (Exception ex) {

        }
    }

    public String aplicarRegex(String texto, String regex) {
        String resultado = "";

        Pattern ptn = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);

        Matcher matcher = ptn.matcher(texto);

        while (matcher.find()) {
            resultado = matcher.group();
        }

        return resultado;
    }

    public boolean encontrouNomeAposAssinatura(String textoAnalisar, String nome) {
        if (StringUtils.isEmpty(textoAnalisar) || StringUtils.isEmpty(nome)) return false;
        else {
            int tamMaiorPalavra = Math.max(nome.length(), textoAnalisar.length());

            int tamCorte = (int) (tamMaiorPalavra * (1.00 - PC_SIMILARIDADE_NOME_E_ASSINATURA));

            String menorPalavra = nome.length() == tamMaiorPalavra ? textoAnalisar : nome;
            String maiorPalavra = nome.length() == tamMaiorPalavra ? nome : textoAnalisar;

            return LevenshteinDistance.getDefaultInstance().apply(menorPalavra.toUpperCase(), maiorPalavra.toUpperCase()) <= tamCorte;
        }
    }

    public boolean assinaturaParecidaComNome(String nome, String assinatura) {
        if ((nome == null && assinatura == null) || (StringUtils.isNotEmpty(nome) && StringUtils.isNotEmpty(assinatura) && nome.equalsIgnoreCase(assinatura)))
            return true;
        else {
            nome = nome == null ? "" : nome;
            assinatura = assinatura == null ? "" : assinatura;

            int tamMaiorPalavra = Math.max(nome.length(), assinatura.length());

            int tamCorte = (int) (tamMaiorPalavra * (1.00 - PC_SIMILARIDADE));

            String menorPalavra = nome.length() == tamMaiorPalavra ? assinatura : nome;
            String maiorPalavra = nome.length() == tamMaiorPalavra ? nome : assinatura;

            return LevenshteinDistance.getDefaultInstance().apply(menorPalavra.toUpperCase(), maiorPalavra.toUpperCase()) <= tamCorte;
        }
    }

    public String aplicarOcr(byte[] dados, BufferedImage bim) {
        try {
            List<AnnotateImageRequest> requests = new ArrayList<>();

            ByteString imgBytes;

            try (ByteArrayOutputStream baos = new ByteArrayOutputStream(); FileOutputStream fos = new FileOutputStream(new File("c:\\dev\\testelaudo.jpeg"))) {
                ImageIO.write(bim, "jpeg", baos);

                ImageIO.write(bim, "jpeg", fos);

                System.out.println("Lendo arquivo com Google");

                if (baos.size() == 0) imgBytes = ByteString.copyFrom(dados);
                else imgBytes = ByteString.copyFrom(baos.toByteArray());

            }

            String texto;
            TextAnnotation annotation = null;

            Image img = Image.newBuilder().setContent(imgBytes).build();
            Feature feat = Feature.newBuilder().setType(Feature.Type.TEXT_DETECTION).build();

            Credentials myCredentials = ServiceAccountCredentials.fromStream(new FileInputStream("Chave_Google/rm32-validacaodocumentos-e0a1e19557ed.json"));
            ImageAnnotatorSettings imageAnnotatorSettings = ImageAnnotatorSettings.newBuilder().setCredentialsProvider(FixedCredentialsProvider.create(myCredentials)).build();

            AnnotateImageRequest request = AnnotateImageRequest.newBuilder().addFeatures(feat).setImage(img).build();
            requests.add(request);

            int yAssinatura = 0;

            try (ImageAnnotatorClient client = ImageAnnotatorClient.create(imageAnnotatorSettings)) {
                BatchAnnotateImagesResponse response = client.batchAnnotateImages(requests);
                List<AnnotateImageResponse> responses = response.getResponsesList();
                client.close();

                for (AnnotateImageResponse res : responses) {
                    if (res.hasError()) {
                        texto = "Error: %s\n" + res.getError().getMessage();
                        return texto;
                    }

                    annotation = res.getFullTextAnnotation();

                    System.out.println(annotation.getAllFields());

                    for (EntityAnnotation entity : res.getTextAnnotationsList()) {
//                        if (!entity.toString().toUpperCase().contains(arquivo.getName().toUpperCase()
//                                .replace(".pdf", ""))) {
//                            continue;
//                        }
                        int vertexY = entity.getBoundingPoly().getVertices(0).getY();
                        yAssinatura = Math.max(vertexY, yAssinatura);
                    }
                }

                return Objects.nonNull(annotation) && StringUtils.isNotEmpty(annotation.getText()) ? annotation.getText() + "y: " + yAssinatura : "";
            }
        } catch (Exception ex) {
            ExceptionUtils.getStackTrace(ex);
        }

        return "";
    }

    public BufferedImage pdfParaImagem(byte[] dados, int pagina) throws IOException {
        try (PDDocument doc = PDDocument.load(dados)) {
            PDFRenderer pdfRenderer = new PDFRenderer(doc);
            BufferedImage bim = pdfRenderer.renderImageWithDPI(pagina, 300, ImageType.RGB);

            return bim;
        }
    }

    public String extrairTextoCompletoPDF(File arquivo) throws IOException {
        byte[] dados = Files.readAllBytes(arquivo.toPath());

        try (PDDocument doc = PDDocument.load(dados)) {
            if (doc.getNumberOfPages() > 0) {
                StringBuilder sbTextoPdf = new StringBuilder();

                for (int i = 0; i < doc.getNumberOfPages(); i++) {
                    try {
                        sbTextoPdf.append("\n").append(aplicarOcr(dados, pdfParaImagem(dados, i)));
                    } catch (Exception ex) {
                        System.out.println("Falha ao extrair texto do PDF " + arquivo.getName() + "!" + "Detalhes: " + ExceptionUtils.getStackTrace(ex));
                    }
                }

                return sbTextoPdf.toString().replaceFirst("\n", "");
            }
        }

        return "";
    }

    public void gerarPlanilhaResultados() {
        List<TesteFichasModel> listTesteFichas = testeFichasService.findAll();

        if (CollectionUtils.isEmpty(listTesteFichas)) {
            System.out.println("Não foram encontradas fichas!");

            return;
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

        String nomeArquivo = formatter.format(LocalDateTime.now()) + ".xlsx";

        File arquivoPlanilha = new File(diretorioResultado + File.separator + nomeArquivo);

        try (FileOutputStream out = new FileOutputStream(arquivoPlanilha)) {
            out.write(0);

            SpreadsheetWriter writer = new SpreadsheetWriter(arquivoPlanilha.getAbsolutePath());

            writer.addSheet(TesteFichasModel.class, listTesteFichas);
            writer.write();
        } catch (Exception ex) {
            System.out.println("Falha ao gerar planilha de resultados! Detalhes: " + ExceptionUtils.getStackTrace(ex));
        }
    }
}
