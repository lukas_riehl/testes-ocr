package br.com.yanksolutions.testegooglecloudvision.testeocr.service;

/*
 * @criado em 15/12/2020 - 12:52
 * @autor Lukas Riehl Figueiredo
 */

import org.springframework.beans.BeanUtils;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.MappedSuperclass;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@MappedSuperclass
public abstract class AbstractService<T, ID> {

    protected abstract JpaRepository<T, ID> getRepository();

    public void saveOrUpdate(ID id, T t) {
        T saved = null;

        if (Objects.nonNull(id)) {
            Optional<T> optional = getRepository().findById(id);

            saved = optional.isPresent() ? optional.get() : null;
        }

        if (saved != null) {
            BeanUtils.copyProperties(t, saved, "id");
            getRepository().saveAndFlush(saved);
        } else {
            getRepository().saveAndFlush(t);
        }
    }

    public void updateAll(List<T> t) {
        getRepository().saveAll(t);
    }
}
