package br.com.yanksolutions.testegooglecloudvision.testeocr.factory;


import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
import com.gargoylesoftware.htmlunit.WebClient;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

@Service
public class WebClientFactory {

    private WebClient browser;

    private void setBrowser() {
        browser = new WebClient(BrowserVersion.CHROME);
        browser.getOptions().setThrowExceptionOnScriptError(false);
        browser.getOptions().setThrowExceptionOnFailingStatusCode(true);
        browser.getOptions().setRedirectEnabled(true);
        browser.getOptions().setJavaScriptEnabled(false);
        browser.setAjaxController(new NicelyResynchronizingAjaxController());
        browser.getOptions().setCssEnabled(false);
        browser.getOptions().setUseInsecureSSL(true);
        browser.getOptions().setTimeout(120000);
    }

    @Bean
    public WebClient getWebClient() {
        if (browser == null) {
            setBrowser();
        }
        return browser;
    }
}
