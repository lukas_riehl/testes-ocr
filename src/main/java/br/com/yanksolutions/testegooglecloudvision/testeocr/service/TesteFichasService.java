package br.com.yanksolutions.testegooglecloudvision.testeocr.service;

import br.com.yanksolutions.testegooglecloudvision.testeocr.model.TesteFichasModel;
import br.com.yanksolutions.testegooglecloudvision.testeocr.repository.TesteFichasRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Transactional
@Service
public class TesteFichasService extends AbstractService<TesteFichasModel, Long> {

    @Autowired
    private TesteFichasRepository testeFichasRepository;

    @Override
    protected JpaRepository<TesteFichasModel, Long> getRepository() {
        return testeFichasRepository;
    }

    public TesteFichasModel findDistinctByCpfCnpj(String cpfCnpj) {
        if (StringUtils.isEmpty(cpfCnpj)) {
            return null;
        } else {
            Optional<TesteFichasModel> optTesteFichasModel = testeFichasRepository.findDistinctByCpfCnpj(cpfCnpj);

            return optTesteFichasModel.orElse(new TesteFichasModel());
        }
    }

    public List<TesteFichasModel> findAll() {
        return testeFichasRepository.findAll();
    }

    public TesteFichasModel findDistinctByCaminhoArquivo(String caminhoArquivo){
        if (StringUtils.isEmpty(caminhoArquivo)) {
            return null;
        } else {
            Optional<TesteFichasModel> optTesteFichasModel = testeFichasRepository
                    .findDistinctByCaminhoArquivo(caminhoArquivo);

            return optTesteFichasModel.orElse(new TesteFichasModel());
        }
    }
}
