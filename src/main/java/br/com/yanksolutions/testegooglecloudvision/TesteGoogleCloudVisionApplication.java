package br.com.yanksolutions.testegooglecloudvision;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.AbstractEnvironment;

@ComponentScan({"br.com.yanksolutions.testegooglecloudvision"})
@SpringBootApplication
public class TesteGoogleCloudVisionApplication {

	public static void main(String[] args) {
        System.setProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME, "prod");

		SpringApplication.run(TesteGoogleCloudVisionApplication.class, args);
	}

}
