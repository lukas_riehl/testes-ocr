package br.com.yanksolutions.testegooglecloudvision.thread;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;

import java.util.Arrays;

public class WebUtils {

    private WebUtils() {
        throw new IllegalStateException("Não foi possível instanciar a classe " + this.getClass().getName() + ", pois ela é um utilitário!");
    }

    public static WebDriver getWebDriver() {
        ChromeOptions options = new ChromeOptions();

        options.addArguments("--start-maximized");
        options.addArguments("--disable-infobars");
        options.addArguments("--disable-extensions");
        options.addArguments("--disable-session-crashed-bubble");
        options.addArguments("--disable-features=InfiniteSessionRestore");
        options.addArguments("--disable-web-security");
        options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        options.addArguments("--disable-application-cache");
        options.addArguments(String.format("--user-agent=%s", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 " + "(KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36"));
        options.setExperimentalOption("excludeSwitches", Arrays.asList("enable-automation", "ignore-certificate-errors", "safebrowsing-disable-download-protection", "safebrowsing-disable-auto-update", "enable-logging", "disable-client-side-phishing-detection"));
        options.addArguments("--disable-blink-features");
        options.addArguments("--disable-blink-features=AutomationControlled");
        options.addArguments("--profile-directory=Default");
        options.addArguments("--disable-plugins-discovery");
        options.addArguments("--log-level=OFF");
        options.addArguments("--headless");

        //System.setProperty("webdriver.chrome.driver", caminhoDriver);

        WebDriverManager.chromedriver().setup();

        WebDriver driver = new ChromeDriver(options);

        return driver;
    }
}
