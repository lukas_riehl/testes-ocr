package br.com.yanksolutions.testegooglecloudvision.thread;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class Rfr02Thread extends Thread {

    private WebDriver webDriver;

    private WebDriverWait webDriverWait;

    public Rfr02Thread(WebDriver webDriver) {
        this.webDriver = webDriver;
        this.webDriverWait = new WebDriverWait(webDriver, Duration.ofSeconds(10L));
    }

    public void acessarSite(String url) {
        this.webDriver.get(url);
    }

    public void enviarTexto(By expressaoBy, String texto) {
        WebElement elemento = this.webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(expressaoBy));

        elemento.sendKeys(texto);
    }

    public void clicarEmBotao(By expressaoBy) {
        WebElement btn = this.webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(expressaoBy));

        ((JavascriptExecutor) this.webDriver).executeScript("arguments[0].click();", btn);
    }

    public String obterTextoElemento(By expressaoBy) {
        WebElement elemento = this.webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(expressaoBy));

        return elemento.getText();
    }

    public void waitForAjax() {
        try {
            WebDriverWait driverWait = new WebDriverWait(webDriver, Duration.ofSeconds(10L));

            ExpectedCondition<Boolean> expectation;
            expectation = new ExpectedCondition<Boolean>() {

                public Boolean apply(WebDriver driverjs) {

                    JavascriptExecutor js = (JavascriptExecutor) driverjs;
                    return js.executeScript("return((window.jQuery != null) && (jQuery.active === 0))").equals("true");
                }
            };
            driverWait.until(expectation);
        }
        catch (TimeoutException exTimeout) {
            System.out.println(ExceptionUtils.getStackTrace(exTimeout));
        }
        catch (WebDriverException exWebDriverException) {
            System.out.println(ExceptionUtils.getStackTrace(exWebDriverException));
        }
    }

    @Override
    public void run() {
        try {
            acessarSite("https://frigol.foodsafe.info/?pais=55&sku=B4016&lg=pt-br&sif=2960" +
                    "&dp=20220125&lp=0000147949&qr=0000147949&v=a71cbf38-fd29-479d-96ea-9249ec984ec6");

            //waitForAjax();

            clicarEmBotao(By.id("btn_historico_produto"));

            //waitForAjax();

            System.out.println(Thread.currentThread().getName() + " "
                    + obterTextoElemento(By.id("TemplatePage")));
        } catch (Exception ex) {
            System.out.println(ExceptionUtils.getStackTrace(ex));
        } finally {
            webDriver.quit();
        }
    }
}
